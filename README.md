# Zotero 插件市场

## 介绍

到 Zotero 插件市场，找到你需要的插件。

#### 插件列表

- 如以下插件未能及时更新或有其他插件需求和推荐，请提交 issue

# [**|-> 提交新插件 OR 催更<-|**](https://gitee.com/qnscholar/zotero-plugins-market/issues)

| 插件名称                | 主要功能                                                                   | 兼容性 | 官网                                                                       | 国内                                                                                                                    |
| ----------------------- | -------------------------------------------------------------------------- | ------ | -------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| 👍Zotero IF             | 更新影响因子、中科院分区、添加 /unread 待读标签...                         | 通用   | [官网](https://gitee.com/qnscholar/zotero-if)                              | [镜像](https://gitee.com/qnscholar/zotero-if/releases)                                                                  |
| 👍Jasminum              | 抓取知网文献元数据、更新翻译器等                                           | 通用   | [官网](https://github.com/l0o0/jasminum/releases)                          | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/jasminum@linxzh.com.xpi)                  |
| 👍ZoteroUpdateIF        | 获取 IF/复合因子/综合因子/南北核心/科技核心/星标...                        | 通用   | [官网](https://github.com/redleafnew/zotero-updateifs/releases)            | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zoteroupdateifs@redleafnew.me.xpi)                 |
| 👍Better BibTex         | 更轻松地管理书目数据                                                       | 通用   | [官网](https://github.com/retorquere/zotero-better-bibtex/releases)        | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zotero-better-bibtex-6.5.1.xpi)       |
| 👍Zutilo                | Zotero 快捷键增强及小工具                                                  | 通用   | [官网](https://github.com/wshanks/Zutilo/releases)                         | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zutilo@www.wesailatdawn.com.xpi)                           |
| 👍Zotero report         | 元数据报告预览                                                             | 通用   | [官网](https://github.com/retorquere/zotero-report-customizer/releases)    | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/report-customizer@iris-advies.com.xpi)  |
| Notero                  | Zotero_Roam 联动 + notion 联动                                             | 通用   | [官网](https://github.com/dvanoni/notero/releases)                         | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/notero@vanoni.dev.xpi)                     |
| Zotcard                 | Zotero 添加卡片笔记                                                        | 通用   | [官网](https://github.com/018/zotcard/releases)                            | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zoterozotcard@018.ai.xpi)                    |
| 👍PDF-Preview        | 添加PDF预览                                                               | 通用   | [官网](https://github.com/dcartertod/zotero-plugins/releases)              | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/pdfpreview@windingwind.com.xpi)                    |
| 👍Quicklook 及相关插件  | Quicklook 及部分插件                                                       | 通用   | [官网](https://github.com/QL-Win/QuickLook/releases)                       | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/QuicklookQuickLook-3.7.1.msi)         |
| 👍Mdnotes               | 将条目元数据导出至 Markdown                                                | 通用   | [官网](https://github.com/argenos/zotero-mdnotes/releases)                 | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/mdnotes@mdnotes.github.io.xpi)                    |
| 👍Delitem               | 删除条目时同时删除链接的附件                                               | 通用   | [官网](https://github.com/redleafnew/delitemwithatt/releases)              | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/delitemwithatt@redleaf.me.xpi)                   |
| ZoteroObsidianCitations | Zotero 内拉起 Obsidian 内的 md 文件                                        | 通用   | [官网](https://github.com/daeh/zotero-obsidian-citations/releases)         | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zotero-obsidian-citations-0.0.15.xpi) |
| 👍Zotfile               | 重命名附件，搭配同步盘实现附件异地存储/同步/整理                           | 通用   | [官网](https://github.com/jlegewie/zotfile/releases)                       | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zotfile@columbia.edu.xpi)                 |
| Zotero PDF Translate    | 在 Zotero 内置阅读器中翻译选中的文字                                       | 通用   | [官网](https://github.com/windingwind/zotero-pdf-translate/releases)       | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zoteropdftranslate@euclpts.com.xpi)             |
| DOI Manager             | 为条目补充 DOI 信息                                                        | 通用   | [官网](https://github.com/bwiernik/zotero-shortdoi/releases)               | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zoteroshortdoi@wiernik.org.xpi)         |
| Duplicates Merger       | 智能合并选中条目，批量合并重复条目                                         | 通用   | [官网](https://github.com/frangoud/ZoteroDuplicatesMerger)                 | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/frangoudes.fotos@gmail.com.xpi)    |
| ZoteroQuickLook Reload  | 在文献列表中使用快捷键预览 PDF （非原作者接手维护）                        | 通用   | [官网](https://github.com/404neko/ZoteroQuickLookReload/releases)          | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zoteroquicklook.zoteroplugin)         |
| ZoteroQuickLook         | 在文献列表中使用快捷键预览 PDF（原作者，已停止维护）                       | ` 5.0` | [官网](https://github.com/mronkko/ZoteroQuickLook/releases)                | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/zoteroquicklookold.zoteroplugin)      |
| MarkDBConnect           | 在用户定义的文件夹中搜索 BibTeX citekey 或 Zotero 项目的标记并添加彩色标记 | 通用   | [官网](https://github.com/daeh/zotero-markdb-connect/releases/tag/v0.0.18) | [镜像](https://gitee.com/qnscholar/zotero-plugins-market/blob/master/PluginMirror/daeda@mit.edu.xpi)                    |

**注:👍 为推荐**
